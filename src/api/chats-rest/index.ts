import type { AxiosInstance } from 'axios'

import { BasicRest } from '../BasicRest'
import type { WithId } from '../types'
import type {
  ChatCreateParams,
  ChatCreateResponse,
  ChatList,
  ChatMessage,
  ChatMessageCreateParams,
  ChatMessageList,
  ChatResponse,
} from './types'

export class ChatsRest extends BasicRest {
  public constructor(endpoint: AxiosInstance) {
    super(endpoint)
  }

  public create(params: ChatCreateParams) {
    return this.postRequest<ChatCreateResponse>('/chats/', params)
  }

  public list() {
    return this.getRequest<{ chats: ChatList }>('/chats/')
  }

  public getChat(params: WithId) {
    return this.getRequest<ChatResponse>(`/chats/${params.id}/`)
  }

  public getChatMessages(params: WithId) {
    return this.getRequest<{ messages: ChatMessageList }>(`/chats/${params.id}/messages/`)
  }

  public createMessage(params: ChatMessageCreateParams) {
    return this.postRequest(`/chats/${params.chatId}/messages/`, { text: params.message })
  }
}
