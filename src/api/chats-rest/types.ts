export type ChatCreateParams = {
  user: string
}

export type ChatCreateResponse = {
  chatId: string
}

export type ChatResponse = {
  id: string
  firstUser: {
    username: string
    avatar?: string
  }
  secondUser: {
    username: string
    avatar?: string
  }
  createdAt: string
  updatedAt: string
}

export type ChatList = ChatResponse[]

export type ChatMessage = {
  chatId: string
  sender: string
  text: string
  createdAt: string
  updatedAt: string
}

export type ChatMessageCreateParams = {
  chatId: string
  message: string
}

export type ChatMessageList = ChatMessage[]
