import type { AxiosInstance } from 'axios'

import { BasicRest } from '../BasicRest'
import type { WithId } from '../types'
import type { User, UserCreateParams, UserUpdateMeParams, UserUpdatePasswordParams } from './types'

export class UsersRest extends BasicRest {
  public constructor(endpoint: AxiosInstance) {
    super(endpoint)
  }

  public create(params: UserCreateParams) {
    return this.postRequest('/users/', params)
  }

  public getOneById(params: WithId) {
    return this.getRequest<User>(`/users/${params.id}`)
  }

  public getMe() {
    return this.getRequest<User>('/users/me/')
  }

  public uploadAvatar(avatar: File) {
    return this.postFormRequest('/users/avatar/', { avatar })
  }

  public updateMe(params: UserUpdateMeParams) {
    return this.patchRequest<User>('/users/me/', params)
  }

  public updatePassword(params: UserUpdatePasswordParams) {
    return this.postRequest('/users/new-password/', params)
  }
}
