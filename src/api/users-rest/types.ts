import type { WithId } from '../types'

export type UserCreateParams = {
  email: string
  username: string
  password: string
}

export type User = {
  aboutMe: string
  education: string
  zodiacSign: string
  hobbies: string
  city: string
  country: string
  jobTitle: string
  height: string
  gender: string
  birthday?: string
  middleName: string
  lastName: string
  firstName: string
  avatar?: string
  username: string
  phoneNumber: string
  email: string
} & WithId

export type UserUpdateMeParams = Partial<Omit<User, 'id' | 'avatar'>>

export type UserUpdatePasswordParams = {
  oldPassword: string
  newPassword: string
}
