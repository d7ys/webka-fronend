export type WithId = {
  id: string
}

export type WithDateAt = {
  createdAt: string
  updatedAt: string
}
