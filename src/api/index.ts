import { type RemovableRef, useLocalStorage } from '@vueuse/core'
import type { AxiosError, AxiosInstance, CreateAxiosDefaults } from 'axios'
import axios from 'axios'

import { ACCESS_TOKEN_KEY } from '@/symbols'

import { AuthRest } from './auth-rest'
import { ChatsRest } from './chats-rest'
import { LikesRest } from './likes-rest'
import { RecommendationRest } from './recommendation-rest'
import { UsersRest } from './users-rest'

export class Api {
  private readonly instance: AxiosInstance

  private readonly accessToken: RemovableRef<string>

  public readonly auth: AuthRest
  public readonly users: UsersRest
  public readonly recommendation: RecommendationRest
  public readonly likes: LikesRest
  public readonly chats: ChatsRest

  public constructor() {
    this.accessToken = useLocalStorage(ACCESS_TOKEN_KEY, '')

    this.instance = Api.createInstance({
      baseURL: new URL('/api/', import.meta.env.VITE_API_URL).toJSON(),
    })

    this.setRequestInterceptor()
    this.setResponseInterceptor()

    this.auth = new AuthRest(this.instance)
    this.users = new UsersRest(this.instance)
    this.recommendation = new RecommendationRest(this.instance)
    this.likes = new LikesRest(this.instance)
    this.chats = new ChatsRest(this.instance)
  }

  public static createInstance(config?: CreateAxiosDefaults) {
    return axios.create({
      ...config,
    })
  }

  private setRequestInterceptor() {
    this.instance.interceptors.request.use((config) => {
      if (this.accessToken.value) {
        config.headers.setAuthorization(`Bearer ${this.accessToken.value}`)
      }

      return config
    })
  }

  private setResponseInterceptor() {
    this.instance.interceptors.response.use(
      (response) => response,
      (error: AxiosError) => {
        return Promise.reject(error)
      },
    )
  }
}

const api = new Api()
const useApi = () => api

export { useApi }
