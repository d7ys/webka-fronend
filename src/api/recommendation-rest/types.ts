import type { WithId } from '../types'

export type Recommendation = {
  gender: string
  birthday: string
  middleName: string
  lastName: string
  firstName: string
  username: string
  avatar: string
} & WithId

export type RecommendationList = Recommendation[]
