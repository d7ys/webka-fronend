import type { AxiosInstance } from 'axios'

import { BasicRest } from '../BasicRest'
import type { RecommendationList } from './types'

export class RecommendationRest extends BasicRest {
  public constructor(endpoint: AxiosInstance) {
    super(endpoint)
  }

  public list() {
    return this.getRequest<RecommendationList>('/recommendation/')
  }
}
