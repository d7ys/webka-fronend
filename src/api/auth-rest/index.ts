import type { AxiosInstance } from 'axios'

import { BasicRest } from '../BasicRest'
import type { AuthParams, AuthResponse } from './types'

export class AuthRest extends BasicRest {
  public constructor(endpoint: AxiosInstance) {
    super(endpoint)
  }

  public getToken(params: AuthParams) {
    return this.postRequest<AuthResponse>('/auth/token/', params)
  }
}
