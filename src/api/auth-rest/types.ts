export type AuthParams = {
  login: string
  password: string
}

export type AuthResponse = {
  access: string
}
