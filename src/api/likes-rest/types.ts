export type LikeCreateParams = {
  user: string
  liked: boolean
}

export type Like = {
  createdBy: {
    id: string
    username: string
    avatar?: string
  }
  createdAt: string
  updatedAt: string
}

export type LikeList = Like[]
