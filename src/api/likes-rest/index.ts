import type { AxiosInstance } from 'axios'

import { BasicRest } from '../BasicRest'
import type { LikeCreateParams, LikeList } from './types'

export class LikesRest extends BasicRest {
  public constructor(endpoint: AxiosInstance) {
    super(endpoint)
  }

  public list() {
    return this.getRequest<LikeList>('/likes/')
  }

  public like(params: LikeCreateParams) {
    return this.postRequest('/likes/', params)
  }
}
