import type { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'

export abstract class BasicRest {
  protected patchRequest<T = void>(url: string, params?: object, config?: AxiosRequestConfig) {
    return BasicRest.dataExtract<T>(this.endpoint.patch<T>(url, params, config))
  }

  protected patchFormRequest<T = void>(url: string, params?: object, config?: AxiosRequestConfig) {
    return BasicRest.dataExtract<T>(this.endpoint.patchForm<T>(url, params, config))
  }

  protected putRequest<T = void>(url: string, params?: object, config?: AxiosRequestConfig) {
    return BasicRest.dataExtract<T>(this.endpoint.put<T>(url, params, config))
  }

  protected putFormRequest<T = void>(url: string, params?: object, config?: AxiosRequestConfig) {
    return BasicRest.dataExtract<T>(this.endpoint.putForm<T>(url, params, config))
  }

  protected postRequest<T = void>(url: string, params?: object, config?: AxiosRequestConfig) {
    return BasicRest.dataExtract<T>(this.endpoint.post<T>(url, params, config))
  }

  protected postFormRequest<T = void>(url: string, params?: object, config?: AxiosRequestConfig) {
    return BasicRest.dataExtract<T>(this.endpoint.postForm<T>(url, params, config))
  }

  protected getRequest<T = void>(url: string, params?: object, config?: AxiosRequestConfig) {
    return BasicRest.dataExtract<T>(this.endpoint.get<T>(url, { params, ...config }))
  }

  protected deleteRequest<T = void>(url: string, params?: object, config?: AxiosRequestConfig) {
    return BasicRest.dataExtract<T>(this.endpoint.delete<T>(url, { params, ...config }))
  }

  protected constructor(protected readonly endpoint: AxiosInstance) {}

  private static async dataExtract<T>(request: Promise<AxiosResponse<T>>) {
    const { data } = await request

    return data
  }
}
