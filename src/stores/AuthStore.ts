import { useLocalStorage } from '@vueuse/core'
import { format as dateFormat } from 'date-fns'
import { defineStore } from 'pinia'
import { computed, reactive, ref } from 'vue'
import { useRouter } from 'vue-router'

import { useApi } from '@/api'
import type { AuthParams } from '@/api/auth-rest/types'
import type { User } from '@/api/users-rest/types'
import { RouteNames } from '@/router/types'
import { ACCESS_TOKEN_KEY } from '@/symbols'

export type AuthState = {
  id: string
  aboutMe: string
  education: string
  zodiacSign: string
  hobbies: string
  city: string
  country: string
  jobTitle: string
  height: string
  gender: string
  birthday: string
  middleName: string
  lastName: string
  firstName: string
  avatar: string
  username: string
  phoneNumber: string
  email: string
}

export const useAuthStore = defineStore('AuthStore', () => {
  const api = useApi()
  const router = useRouter()

  const accessToken = useLocalStorage(ACCESS_TOKEN_KEY, '')

  const authState = reactive<AuthState>({
    id: '',
    aboutMe: '',
    education: '',
    zodiacSign: '',
    hobbies: '',
    city: '',
    country: '',
    jobTitle: '',
    height: '',
    gender: '',
    birthday: '',
    middleName: '',
    lastName: '',
    firstName: '',
    avatar: '',
    username: '',
    phoneNumber: '',
    email: '',
  })

  const isAuthenticated = computed(() => !!authState.id)
  const isInitialized = ref(false)

  async function initialize() {
    try {
      if (!accessToken.value) {
        throw new Error('[AuthStore]: AccessToken empty')
      }

      const response = await api.users.getMe()

      updateState(response)
    } catch (error) {
      console.error(error)
    } finally {
      isInitialized.value = true
    }
  }

  function logOut() {
    accessToken.value = ''
    updateState()

    return router.replace({
      name: RouteNames.SIGN_IN,
    })
  }

  async function signIn(params: AuthParams) {
    const response = await api.auth.getToken(params)

    accessToken.value = response.access

    return initialize()
  }

  function updateState(data?: User) {
    if (!data) {
      authState.id = ''
      authState.aboutMe = ''
      authState.education = ''
      authState.zodiacSign = ''
      authState.hobbies = ''
      authState.city = ''
      authState.country = ''
      authState.jobTitle = ''
      authState.height = ''
      authState.gender = ''
      authState.birthday = ''
      authState.middleName = ''
      authState.lastName = ''
      authState.firstName = ''
      authState.avatar = ''
      authState.username = ''
      authState.phoneNumber = ''
      authState.email = ''

      return
    }

    authState.id = data.id
    authState.aboutMe = data.aboutMe
    authState.education = data.education
    authState.zodiacSign = data.zodiacSign
    authState.hobbies = data.hobbies
    authState.city = data.city
    authState.country = data.country
    authState.jobTitle = data.jobTitle
    authState.height = data.height
    authState.gender = data.gender
    authState.birthday = data.birthday ? dateFormat(new Date(data.birthday), 'yyyy-MM-dd') : ''
    authState.middleName = data.middleName
    authState.lastName = data.lastName
    authState.firstName = data.firstName
    authState.avatar = data.avatar ?? ''
    authState.username = data.username
    authState.phoneNumber = data.phoneNumber
    authState.email = data.email
  }

  return {
    authState: computed(() => authState),

    isAuthenticated,
    isInitialized: computed(() => isInitialized.value),

    updateState,
    initialize,
    logOut,

    signIn,
  }
})
