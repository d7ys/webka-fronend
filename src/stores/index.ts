import { createPinia } from 'pinia'

const store = createPinia()

export * from './AuthStore'

export { store }
