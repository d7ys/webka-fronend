import { storeToRefs } from 'pinia'

import { useAuthStore } from '@/stores'

export function useCurrentUser() {
  const authStore = useAuthStore()
  const { authState } = storeToRefs(authStore)

  return {
    ...authState.value,
  }
}
