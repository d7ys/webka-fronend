import { storeToRefs } from 'pinia'
import type { NavigationGuardNext, RouteLocationNormalized } from 'vue-router'

import { useAuthStore } from '@/stores'

import { LOCATE_TO_QUERY } from '..'
import { RouteNames } from '../types'

async function authGuard(
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext,
) {
  const authStore = useAuthStore()

  const { isInitialized, isAuthenticated } = storeToRefs(authStore)

  if (!isInitialized.value) {
    await authStore.initialize()
  }

  if (isAuthenticated.value) {
    return next()
  }

  switch (to.name) {
    case RouteNames.SIGN_IN:
      return next()
    case RouteNames.SIGN_UP:
      return next()
  }

  return next({
    name: RouteNames.SIGN_IN,
    query: {
      [LOCATE_TO_QUERY]: to.fullPath,
    },
  })
}

export { authGuard }
