import { createRouter, createWebHistory } from 'vue-router'

import { authGuard } from './guards/auth-guard'
import { RouteNames } from './types'

export const LOCATE_TO_QUERY = 'locateTo'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      children: [
        {
          children: [
            {
              name: RouteNames.SIGN_IN,
              path: 'sign-in',
              component: () => import('@/pages/SignIn.vue'),
            },
            {
              name: RouteNames.SIGN_UP,
              path: 'sign-up',
              component: () => import('@/pages/SignUp.vue'),
            },
          ],
          path: '/auth',
          component: () => import('@/layouts/AuthLayout.vue'),
          redirect: (route) => ({ ...route, name: RouteNames.SIGN_IN }),
        },

        {
          children: [
            {
              name: RouteNames.WELCOME,
              path: '',
              component: () => import('@/pages/TheWelcome.vue'),
            },
            {
              name: RouteNames.LIKES,
              path: 'likes',
              component: () => import('@/pages/TheLikes.vue'),
            },
            {
              name: RouteNames.CHATS,
              path: 'chats',
              component: () => import('@/pages/TheChats.vue'),
            },
            {
              name: RouteNames.CHAT_DETAIL,
              path: 'chats/:id/',
              component: () => import('@/pages/TheChat.vue'),
            },
            {
              name: RouteNames.ACCOUNT,
              path: 'account',
              component: () => import('@/pages/TheAccount.vue'),
            },
            {
              name: RouteNames.PROFILE,
              path: 'profile/:id/',
              component: () => import('@/pages/TheProfile.vue'),
            },
            {
              name: RouteNames.PAGE_NOT_FOUND,
              path: '/:pathMatch(.*)*',
              component: () => import('@/pages/PageNotFound.vue'),
            },
          ],
          path: '',
          component: () => import('@/layouts/DashboardLayout.vue'),
        },
      ],
      path: '/',
      component: () => import('@/layouts/BaseLayout.vue'),
      redirect: (route) => ({ ...route, name: RouteNames.SIGN_IN }),
    },
  ],
})

router.beforeEach(authGuard)

export default router
