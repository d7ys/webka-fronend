import { createApp } from 'vue'

import AppEntry from '@/AppEntry.vue'
import router from '@/router'
import { store } from '@/stores'
import '@/styles/index.scss'

const app = createApp(AppEntry)

app.use(store)
app.use(router)

app.mount('#app')

console.log('Build date:', __BUILD_DATE)
