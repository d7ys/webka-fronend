import vue from '@vitejs/plugin-vue'
import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vite'
import viteSvgLoader from 'vite-svg-loader'

// https://vitejs.dev/config/
export default defineConfig({
  define: {
    __BUILD_DATE: JSON.stringify(new Date().toLocaleString('ru-RU', { timeZone: 'Asia/Atyrau' })),
  },
  plugins: [
    vue(),
    viteSvgLoader({
      svgo: false,
    }),
  ],
  build: {
    target: 'esnext',
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
  server: {
    host: '0.0.0.0',
    port: 2525,
    strictPort: true,
  },
})
