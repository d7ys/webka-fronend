/** @type {import("prettier").Config} */
module.exports = {
  importOrder: [
    '^@/assets/(.*)$',
    '^@/components/(.*)$',
    '^@/stores/(.*)$',
    '^@/views/(.*)$',
    '^@/helpers/(.*)$',
    '^(@/.*)',
    '^[./]',
  ],
  importOrderCaseInsensitive: true,
  importOrderGroupNamespaceSpecifiers: true,
  importOrderSortSpecifiers: true,
  importOrderSeparation: true,
  trailingComma: 'all',
  printWidth: 100,
  plugins: ['@trivago/prettier-plugin-sort-imports', 'prettier-plugin-tailwindcss'],
  endOfLine: 'lf',
  singleQuote: true,
  tabWidth: 2,
  semi: false,
}

