/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

/** @type {import('eslint/lib/shared/types').ConfigData} */
module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  extends: [
    'eslint:recommended',

    'plugin:vue/vue3-recommended',

    '@vue/eslint-config-prettier/skip-formatting',
    '@vue/eslint-config-typescript',
  ],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  rules: {
    'linebreak-style': ['error', 'unix'],

    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',

    '@typescript-eslint/no-unused-vars': 'off',

    'vue/attributes-order': [
      'error',
      {
        alphabetical: true,
      },
    ],

    'vue/no-setup-props-destructure': 'off',
    'vue/require-default-prop': 'off',
    'vue/prop-name-casing': 'off',
    'vue/valid-v-for': 'off',

    'no-useless-escape': 'off',
    'prefer-const': 'off',
  },
  globals: {
    defineSlots: 'readonly',
    defineEmits: 'readonly',
    defineExpose: 'readonly',
    defineProps: 'readonly',
    withDefaults: 'readonly',
  },
}
